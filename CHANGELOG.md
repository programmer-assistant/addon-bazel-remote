# Changelog

## 0.4.0

* Upgrade base image to 3.14

## 0.3.3

* Update logo

## 0.3.2

* Remove grpc port
* Update documentation

## 0.3.1

* Fix options set

## 0.3.0

* Add options set

## 0.2.0

* Migrate to multiple arch repositories

## 0.1.0

* Initial build based on bazel-remote v2.0.1
