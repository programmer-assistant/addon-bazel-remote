ARG DEP_PROXY=
FROM ${DEP_PROXY}golang:1.15.15-alpine3.14 as build
ARG TARGETARCH

ENV BAZEL_REMOTE_VERSION "2.1.3"
RUN apk add --no-cache git=2.32.0-r0 bash=5.1.4-r0
WORKDIR /go/src
RUN wget -q https://github.com/buchgr/bazel-remote/archive/refs/tags/v${BAZEL_REMOTE_VERSION}.tar.gz \
    && tar -xf v${BAZEL_REMOTE_VERSION}.tar.gz \
    && mv bazel-remote-${BAZEL_REMOTE_VERSION} bazel-remote
WORKDIR /go/src/bazel-remote
RUN GOPROXY="direct" CGO_ENABLED=0 GOOS=linux GOARCH=$TARGETARCH go build -a -ldflags "-extldflags '-static' -X main.gitCommit=v${BAZEL_REMOTE_VERSION}" .

ARG DEP_PROXY=
FROM ${DEP_PROXY}programmerassistant/base:3.14

# Copy data
COPY rootfs /

# Setup base
COPY --from=build /go/src/bazel-remote/bazel-remote /usr/local/sbin/

# Build arguments
ARG BUILD_DATE
ARG BUILD_DESCRIPTION
ARG BUILD_NAME
ARG BUILD_REF
ARG BUILD_REPOSITORY
ARG BUILD_VERSION

# Labels
LABEL \
    io.hass.name="${BUILD_NAME}" \
    io.hass.description="${BUILD_DESCRIPTION}" \
    io.hass.arch="multiarch" \
    io.hass.type="addon" \
    io.hass.version=${BUILD_VERSION} \
    maintainer="Dawid Rycerz <spam@rycerz.co>" \
    org.opencontainers.image.title="${BUILD_NAME}" \
    org.opencontainers.image.description="${BUILD_DESCRIPTION}" \
    org.opencontainers.image.vendor="Programmer Assistant HASS Add-on" \
    org.opencontainers.image.authors="Dawid Rycerz <spam@rycerz.co>" \
    org.opencontainers.image.licenses="MIT" \
    org.opencontainers.image.url="https://addons.programmer-assistant.io" \
    org.opencontainers.image.source="https://gitlab.com/programmer-assistant/addons/-/tree/master/${BUILD_NAME}" \
    org.opencontainers.image.documentation="https://gitlab.com/programmer-assistant/addons/-/blob/master/${BUILD_NAME}/README.md" \
    org.opencontainers.image.created=${BUILD_DATE} \
    org.opencontainers.image.revision=${BUILD_REF} \
    org.opencontainers.image.version=${BUILD_VERSION}
